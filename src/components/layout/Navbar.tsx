import { Link } from "react-router-dom";
import { useToggle } from "../../hooks/useToggle";
import { Icon } from "@iconify/react";

const links = [
  {
    name: "AI Chat",
  },
  {
    name: "Image Processing",
  },
  {
    name: "Voice Assistant",
  },
  {
    name: "Health Management",
  },
];

const Navbar = () => {
  const open = useToggle(false);

  return (
    <nav className="w-full fixed z-10 backdrop-blur-xl">
      <div className="container py-8 m-0-auto flex justify-between">
        <div className="lg:hidden flex"></div>
        <h6 className="lg:text-4xl text-2xl">
          Empower<span className="orange">AI</span>
        </h6>
        <div className="lg:flex hidden">
          {links.map((link) => (
            <Link
              className="mx-3 text-xl font-extralight"
              key={link.name}
              to={link.name}
            >
              {link.name}
            </Link>
          ))}
        </div>
        <div className="lg:hidden flex cursor-pointer relative">
          <Icon
            icon="ri:menu-3-fill"
            width={25}
            onClick={() => open.toggle()}
          />
          {open.state && (
            <div className="absolute top-10 right-0 p-3 rounded-xl bg-gray-200 w-96 flex flex-col">
              {links.map((link) => (
                <Link
                  className="my-4 text-xl font-extralight"
                  key={link.name}
                  to={link.name}
                >
                  {link.name}
                </Link>
              ))}
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
