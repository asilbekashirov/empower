import { Icon } from "@iconify/react";
import iphone from "../images/phone.png";
import iphone2 from "../images/iphone2.png";
import iphoneMain from "../images/main.png";

const data = [
  {
    head: "AI Chat",
    title: "Virtual Assistant",
    body: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti recusandae et rerum corporis excepturi. Consequatur illo praesentium soluta cupiditate atque minima reprehenderit obcaecati. Velit, eaque rem. Ad cum odit voluptas expedita aspernatur error, totam, in nisi, exercitationem ea facere? Sint, doloribus quia! Maiores iusto illum nobis, quod minus nam! Aperiam natus amet hic, repellendus architecto ea omnis error ullam, at illo molestias dolorem eius mollitia quam autem, minima enim recusandae similique obcaecati voluptatibus! A neque doloribus deserunt voluptatibus placeat aut dolore ad omnis asperiores. Magnam explicabo dolores aliquid, laboriosam a porro nihil deserunt, placeat esse ducimus vitae hic temporibus itaque?",
    icons: [
      {
        icon: "mdi:microphone",
        label: "Voice to text",
        color: "#EC6565",
      },
      {
        icon: "mdi:volume-high",
        label: "Text to voice",
        color: "#EC6565",
      },
      {
        icon: "mdi:brain",
        label: "ChatGPT",
        color: "#00A67E",
      },
    ],
    img: iphone,
    direction: "ltr",
  },
  {
    head: "Image processing",
    title: "Face recognition",
    body: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti recusandae et rerum corporis excepturi. Consequatur illo praesentium soluta cupiditate atque minima reprehenderit obcaecati. Velit, eaque rem. Ad cum odit voluptas expedita aspernatur error, totam, in nisi, exercitationem ea facere? Sint, doloribus quia! Maiores iusto illum nobis, quod minus nam! Aperiam natus amet hic, repellendus architecto ea omnis error ullam, at illo molestias dolorem eius mollitia quam autem, minima enim recusandae similique obcaecati voluptatibus! A neque doloribus deserunt voluptatibus placeat aut dolore ad omnis asperiores. Magnam explicabo dolores aliquid, laboriosam a porro nihil deserunt, placeat esse ducimus vitae hic temporibus itaque?",
    icons: [
      {
        icon: "mdi:emoticon-excited-outline",
        label: "Facial expressions",
        color: "#EC6565",
      },
      {
        icon: "mdi:lock",
        label: "Authentication",
        color: "#222",
      },
    ],
    img: iphone,
    direction: "rtl",
  },
  {
    head: "Gestures",
    title: "Sign language recognition",
    body: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Corrupti recusandae et rerum corporis excepturi. Consequatur illo praesentium soluta cupiditate atque minima reprehenderit obcaecati. Velit, eaque rem. Ad cum odit voluptas expedita aspernatur error, totam, in nisi, exercitationem ea facere? Sint, doloribus quia! Maiores iusto illum nobis, quod minus nam! Aperiam natus amet hic, repellendus architecto ea omnis error ullam, at illo molestias dolorem eius mollitia quam autem, minima enim recusandae similique obcaecati voluptatibus! A neque doloribus deserunt voluptatibus placeat aut dolore ad omnis asperiores. Magnam explicabo dolores aliquid, laboriosam a porro nihil deserunt, placeat esse ducimus vitae hic temporibus itaque?",
    icons: [
      {
        icon: "mdi:database-outline",
        label: "Training model",
        color: "#222",
      },
      {
        icon: "mdi:camera-outline",
        label: "Real time chat",
        color: "#222",
      },
    ],
    img: iphone2,
    direction: "ltr",
  },
];

const LandingPage = () => {
  return (
    <div className="w-full">
      <div className="container lg:px-2 px-6 m-0-auto min-h-screen">
        {/* first block  */}
        <div className="flex flex-col lg:flex-row lg:justify-between justify-center items-center min-h-screen lg:pt-1 pt-32">
          <div className="flex flex-col lg:text-left text-center">
            <h1 className="lg:text-8xl text-6xl">
              Embrace <br /> inclusivity and <br /> unlock new <br />{" "}
              possibilities
            </h1>
            <h3 className="lg:text-4xl text-2xl mt-16">With Empower<span className="orange">AI</span></h3>
            <p className="lg:text-xl text-lg text-center mt-8 text-gray-500 flex items-center">Start the journey <Icon className="ml-1 animate-bounce" width={25} icon="mdi:arrow-down" /></p>
          </div>
          <div className="mt-8 lg:mt-0">
            <img src={iphoneMain} />
          </div>
        </div>
        {/* second block  */}
        <div className="flex flex-col min-h-screen">
          {data.map((item) => (
            <div
              className={`flex ${
                item.direction === "ltr"
                  ? "lg:flex-row flex-col"
                  : "lg:flex-row-reverse flex-col-reverse"
              } justify-between items-center lg:mt-6 mt-16 lg:gap-1 gap-5`}
            >
              <div className="lg:w-3/6 w-full">
                <h5 className="text-3xl">{item.head}</h5>
                <div className="bg-white lg:p-8 p-5 rounded-3xl mt-6">
                  <h6 className="text-2xl orange">{item.title}</h6>
                  <p className="mt-4">{item.body}</p>
                </div>
              </div>
              <div className="lg:w-3/6 w-full flex items-center">
                <div className="w-1/2 lg:mt-0 mt-8 h-full flex flex-col items-center justify-center">
                  {item.icons.map((icon) => (
                    <div className="flex flex-col items-center lg:mt-6 mt-3">
                      <div className="rounded-full w-16 h-16 bg-white flex justify-center items-center">
                        <Icon color={icon.color} icon={icon.icon} width={35} />
                      </div>
                      <p>{icon.label}</p>
                    </div>
                  ))}
                </div>
                <div className="w-1/2 lg:mt-0 mt-6">
                  <img src={item.img} />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default LandingPage;
