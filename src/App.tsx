import Navbar from "./components/layout/Navbar"
import LandingPage from "./pages/LandingPage"

function App() {
  return (
    <>
      <Navbar />
      <LandingPage />
    </>
  )
}

export default App