import { useRoutes } from "react-router-dom";
import LandingPage from "./pages/LandingPage";

export function Router() {
  return useRoutes([
    {
      path: "/",
      element: <LandingPage />,
    },
  ]);
}
